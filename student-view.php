<?php
    include './topbar.php';
?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./dashboard.php">Home</a></li>
        <li class="breadcrumb-item"><a href="./student-index.php">Student Table</a></li>
        <li class="breadcrumb-item active" aria-current="page">Student Profile</li>
    </ol>
</nav>
<div class="container-fluid">


    <div class="mb-4">
        <a class="btn btn-sm btn-primary mr-3" href="./marksheet1.php">View 1st Year Marks</a>
        <a class="btn btn-sm btn-primary ml-3" href="./marksheet2.php">View 2nd Year Marks</a>
    </div>
    <div class="card ">

        <div class="card-body">


            <form>
                <div class="form-group row text-dark">
                    <div class="form-group col-md-6">
                        <label class="col-form-label font-weight-bold">Name of Trainee</label>
                        <input type="text" class="form-control form-control-plaintext w-50" contenteditable="true"
                            value="John Doe">
                    </div>
                    <div class="form-group col-md-6 font-weight-bold">
                        <label class="col-form-label font-weight-bold">Registration No.</label>
                        <input type="text" class="form-control form-control-plaintext w-50" contenteditable="true"
                            value="10056548">
                    </div>
                    <div class="form-group col-md-6 font-weight-bold">
                        <label class="col-form-label font-weight-bold">Gender</label>
                        <input type="text" class="form-control form-control-plaintext w-50" contenteditable="true"
                            value="Male">
                    </div>
                    <div class="form-group col-md-6 font-weight-bold">
                        <label class="col-form-label font-weight-bold">Educational Qualification</label>
                        <input type="text" class="form-control form-control-plaintext w-50" contenteditable="true"
                            value="BA">
                    </div>
                    <div class="form-group col-md-6 font-weight-bold">
                        <label class="col-form-label font-weight-bold">Marks Obtained in HSSLC</label>
                        <input type="text" class="form-control form-control-plaintext w-50" contenteditable="true"
                            value="365">
                    </div>
                    <div class="form-group col-md-6 font-weight-bold">
                        <label class="col-form-label font-weight-bold">Professional Education</label>
                        <input type="text" class="form-control form-control-plaintext w-50" contenteditable="true"
                            value="PGDCA">
                    </div>
                    <div class="form-group col-md-6 font-weight-bold">
                        <label class="col-form-label font-weight-bold">School Type</label>
                        <input type="text" class="form-control form-control-plaintext w-50" contenteditable="true"
                            value="Govt">
                    </div>
                    <div class="form-group col-md-6 font-weight-bold">
                        <label class="col-form-label font-weight-bold">School Sl. No.</label>
                        <input type="text" class="form-control form-control-plaintext w-50" contenteditable="true"
                            value="A542">
                    </div>
                    <div class="form-group col-md-6 font-weight-bold">
                        <label class="col-form-label font-weight-bold">Center Sl. No.</label>
                        <input type="text" class="form-control form-control-plaintext w-50" contenteditable="true"
                            value="AS880">
                    </div>
                </div>
                <div class="text-center float-right">
                    <a class="btn btn-danger" href="#" style="margin-top:-30px">Reset Details</a>
                    <a class="btn btn-primary" href="#" style="margin-top:-30px">Update Details</a>
                </div>

            </form>
        </div>
    </div>
</div>

<?php
    include './footer.php';
?>