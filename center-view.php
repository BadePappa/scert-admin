<?php
    include './topbar.php';
?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./dashboard.php">Home</a></li>
        <li class="breadcrumb-item"><a href="./center-index.php">Center Table</a></li>
        <li class="breadcrumb-item active" aria-current="page">Center Details</li>
    </ol>
</nav>
<div class="container-fluid">
    <div class="card ">
        <div class="card-body">
            <form>
                <div class="form-group row text-dark">
                    <div class="form-group col-md-6">
                        <label class="col-form-label font-weight-bold">Name of TEI</label>
                        <input type="text" class="form-control form-control-plaintext w-50" contenteditable="true"
                            value="Example 1">
                    </div>
                    <div class="form-group col-md-6 font-weight-bold">
                        <label class="col-form-label font-weight-bold">District</label>
                        <input type="text" class="form-control form-control-plaintext w-50" contenteditable="true"
                            value="Kamrup Metro">
                    </div>
                </div>
                <div class="text-center float-right">
                    <a class="btn btn-danger" href="#">Reset Details</a>
                    <a class="btn btn-primary" href="#">Update Details</a>
                </div>

            </form>
        </div>
    </div>
</div>

<?php
    include './footer.php';
?>