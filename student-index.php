<?php
    include './topbar.php';
?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Student Table</li>
    </ol>
</nav>
<div class="container-fluid">



    <div class="card mb-4">
        <div class="card-header py-3">
            <span class="font-weight-bold text-primary">Student Details Table</span>
            <form class="float-right">
                <input class="form-control" type="search" placeholder="Search" aria-label="Search">
            </form>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-sm text-center text-dark" width="100%"
                    cellspacing="0">
                    <thead>
                        <tr class="bg-primary text-white">
                            <th width="8%">Sl. No.</th>
                            <th>Registration No.</th>
                            <th>Name of Trainee</th>
                            <th>Private/Govt School</th>
                            <th>School Sl. No.</th>
                            <th>Center Sl. No.</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>10005654</td>
                            <td>John Doe</td>
                            <td>Govt</td>
                            <td>0556</td>
                            <td>10078</td>
                            <td><a class="btn btn-info btn-sm" href="./student-view.php"><i
                                        class="fa fa-eye"></i>View</a></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>10005654</td>
                            <td>John Doe</td>
                            <td>Private</td>
                            <td>1025</td>
                            <td>15644</td>
                            <td><a class="btn btn-info btn-sm" href="./student-view.php"><i
                                        class="fa fa-eye"></i>View</a></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>10005654</td>
                            <td>John Doe</td>
                            <td>Govt</td>
                            <td>0556</td>
                            <td>10078</td>
                            <td><a class="btn btn-info btn-sm" href="./student-view.php"><i
                                        class="fa fa-eye"></i>View</a></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>10005654</td>
                            <td>John Doe</td>
                            <td>Govt</td>
                            <td>0556</td>
                            <td>10078</td>
                            <td><a class="btn btn-info btn-sm" href="./student-view.php"><i
                                        class="fa fa-eye"></i>View</a></td>
                        </tr>
                    </tbody>
                </table>
                <nav class="float-right">
                    <ul class="pagination">
                        <li class="page-item">
                            <a class="page-link" href="#" tabindex="-1">Previous</a>
                        </li>
                        <li class="page-item active">
                            <a class="page-link" href="#">1<span class="sr-only">(current)</span></a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" href="#">2</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" href="#">3</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" href="#">Next</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>


</div>



<?php
    include './footer.php';
?>