<?php
    include './topbar.php';
?>
<nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./dashboard.php">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Center Table</li>
        </ol>
    </nav>
<div class="container-fluid">
    <div class="card mb-4">
        <div class="card-header py-3">
            <span class="font-weight-bold text-primary">Center Details Table</span>
            <form class="float-right">
                <input class="form-control" type="search" placeholder="Search" aria-label="Search">
            </form>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-sm text-center text-dark" width="100%" cellspacing="0">
                    <thead>
                        <tr class="bg-primary text-white">
                            <th width="8%">Sl. No.</th>
                            <th>Name of TEI</th>
                            <th>District</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Example 1</td>
                            <td>Kamrup Rural</td>
                            <td><a class="btn btn-info btn-sm" href="./center-view.php"><i class="fa fa-edit"></i>Edit</a></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Example 2</td>
                            <td>Bongaigaon</td>
                            <td><a class="btn btn-info btn-sm" href="./center-view.php"><i class="fa fa-edit"></i>Edit</a></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Example 3</td>
                            <td>Dibrugarh</td>
                            <td><a class="btn btn-info btn-sm" href="./center-view.php"><i class="fa fa-edit"></i>Edit</a></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Example 4</td>
                            <td>Sonitpur</td>
                            <td><a class="btn btn-info btn-sm" href="./center-view.php"><i class="fa fa-edit"></i>Edit</a></td>
                        </tr>
                    </tbody>
                </table>
                <nav class="float-right">
                    <ul class="pagination">
                        <li class="page-item">
                            <a class="page-link" href="#" tabindex="-1">Previous</a>
                        </li>
                        <li class="page-item active">
                            <a class="page-link" href="#">1<span class="sr-only">(current)</span></a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" href="#">2</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" href="#">3</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" href="#">Next</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>


</div>



<?php
    include './footer.php';
?>