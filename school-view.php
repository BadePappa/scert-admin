<?php
    include './topbar.php';
?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./dashboard.php">Home</a></li>
        <li class="breadcrumb-item"><a href="./school-index.php">School Table</a></li>
        <li class="breadcrumb-item active" aria-current="page">School Details</li>
    </ol>
</nav>
<div class="container-fluid">
    <div class="card ">
        <div class="card-body">
            <form>
                <div class="form-group row text-dark">
                    <div class="form-group col-md-6">
                        <label class="col-form-label font-weight-bold">Name of School</label>
                        <input type="text" class="form-control form-control-plaintext w-50" contenteditable="true"
                            value="Gopal Boro High School">
                    </div>
                    <div class="form-group col-md-6 font-weight-bold">
                        <label class="col-form-label font-weight-bold">Address</label>
                        <input type="text" class="form-control form-control-plaintext w-50" contenteditable="true"
                            value="Ganeshguri">
                    </div>
                    <div class="form-group col-md-6 font-weight-bold">
                        <label class="col-form-label font-weight-bold">Center Sl. No.</label>
                        <input type="text" class="form-control form-control-plaintext w-50" contenteditable="true"
                            value="AS10556">
                    </div>
                </div>
                <div class="text-center float-right">
                    <a class="btn btn-danger" href="#">Reset Details</a>
                    <a class="btn btn-primary" href="#">Update Details</a>
                </div>

            </form>
        </div>
    </div>
</div>

<?php
    include './footer.php';
?>