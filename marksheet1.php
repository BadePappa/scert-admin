<?php
    include './topbar.php';
?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./dashboard.php">Home</a></li>
        <li class="breadcrumb-item"><a href="./student-index.php">Student Table</a></li>
        <li class="breadcrumb-item"><a href="./student-view.php">Student Profile</a></li>
        <li class="breadcrumb-item active" aria-current="page">1st Year Marksheet</li>
    </ol>
</nav>
<div class="container-fluid">
    <div class="text-dark" style="letter-spacing:0.7px">
        <b>Name: <span class="text-uppercase"><a href="./student-view.php">John Doe</a></span></b><br>
        <b>School's Name:</b> <span class="text-uppercase font-weight-bold">Gopal Boro High School, Ganeshguri</span><br>
        <b>Class:</b> <span class="text-uppercase font-weight-bold">1st Year</span>
    </div>
    <table class="table table-striped table-sm text-dark text-center border mt-5">
        <thead class="text-center">
            <tr>
                <th>Course Code</th>
                <th>External Marks</th>
                <th>Assessment Marks</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>01</td>
                <td contenteditable="true">20</td>
                <td contenteditable="true">65</td>
                <td>85</td>
            </tr>
            <tr>
                <td>02</td>
                <td contenteditable="true">20</td>
                <td contenteditable="true">65</td>
                <td>85</td>
            </tr>
            <tr>
                <td>03</td>
                <td contenteditable="true">20</td>
                <td contenteditable="true">65</td>
                <td>85</td>
            </tr>
            <tr>
                <td>04</td>
                <td contenteditable="true">20</td>
                <td contenteditable="true">65</td>
                <td>85</td>
            </tr>
            <tr>
                <td>05</td>
                <td contenteditable="true">20</td>
                <td contenteditable="true">65</td>
                <td>85</td>
            </tr>
            <tr>
                <td>06</td>
                <td contenteditable="true">20</td>
                <td contenteditable="true">65</td>
                <td>85</td>
            </tr>
            <tr>
                <td>07</td>
                <td contenteditable="true">20</td>
                <td contenteditable="true">65</td>
                <td>85</td>
            </tr>
            <tr>
                <td>08</td>
                <td contenteditable="true">20</td>
                <td contenteditable="true">65</td>
                <td>85</td>
            </tr>
            <tr>
                <td>09</td>
                <td contenteditable="true">20</td>
                <td contenteditable="true">65</td>
                <td>85</td>
            </tr>
        <tfoot>
            <tr>
                <th>Total Marks :</th>
                <th></th>
                <th></th>
                <th>565</th>
            </tr>
        </tfoot>
        </tbody>
    </table>
    <div class="text-dark mt-5" style="letter-spacing:0.7px">
        <b>Result : <span>PASS</span></b>
        <br>
        <b>Percentage : <span>86.6%</span></b>
    </div>
</div>

<?php
    include './footer.php';
?>